static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#ebdbb2", "#282828" },
	[SchemeSel] = { "#000000", "#89b482" },
	[SchemeSelHighlight] = { "#924f79", "#89b482" },
	[SchemeNormHighlight] = { "#d3869b", "#282828" },
	[SchemeOut] = { "#ffffff", "#7daea3" },
	[SchemeMid] = { "#ebdbb2", "#282828" },
};
